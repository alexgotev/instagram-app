# InstagramApp #

Proof-Of-Concept which shows how to implement a basic instagram client app, which syncs user's images directly on the device and periodically checks for updates, notifying the user if new content is available.

### Contents of this repo ###

* Server-side PHP script which implements Instragram's Explicit OAuth flow
* Mobile app for Android 16+

### External libraries ###

* **Retrogram**: a library which wraps Instagram API calls with Retrofit and Gson
* **Glide**: a library to load images in views
* **Android support libraries**: needed to have a consistent user experience across different Android versions
* **OkHttp**: an HTTP stack used to download images from Instagram in the background. It's more efficient than Android's built in `HttpUrlConnection`. So good that starting from Android 4.4, Google used `OkHttp` as the backend to provide `HttpUrlConnection` class.