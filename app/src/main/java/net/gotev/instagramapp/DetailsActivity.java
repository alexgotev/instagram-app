package net.gotev.instagramapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;

/**
 * @author Aleksandar Gotev
 */
public class DetailsActivity extends AppCompatActivity implements OnSwipeTouchListener.Delegate {

    private static final int CROSSFADE_DURATION_MS = 200;
    private static final String PARAM_IMAGE_NAME = "imageName";

    private ImageView imageView;
    private String[] imageFiles = Util.getImageFileNames();
    private int imageIndex;

    public static void open(Context context, String imageName) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(PARAM_IMAGE_NAME, imageName);

        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        imageView = (ImageView) findViewById(R.id.image);
        imageView.setOnTouchListener(new OnSwipeTouchListener(this, this));

        String imageName = getIntent().getStringExtra(PARAM_IMAGE_NAME);

        for (int i = 0; i < imageFiles.length; i++) {
            if (imageName.equals(imageFiles[i])) {
                imageIndex = i;
                break;
            }
        }

        Glide.with(this)
               .load(getImageFile(imageName))
               .into(imageView);
    }

    private File getImageFile(String imageName) {
        return new File(Util.getStorageDir(Util.IMAGES_DIR), imageName);
    }

    private File getNextImage() {
        int tempIndex = imageIndex;
        tempIndex++;

        if (tempIndex >= imageFiles.length)
            imageIndex = 0;
        else
            imageIndex = tempIndex;

        return getImageFile(imageFiles[imageIndex]);
    }

    private File getPreviousImage() {
        int tempIndex = imageIndex;
        tempIndex--;

        if (tempIndex < 0)
            imageIndex = imageFiles.length - 1;
        else
            imageIndex = tempIndex;

        return getImageFile(imageFiles[imageIndex]);
    }

    public void onSwipeLeft() {
        Glide.with(this)
                .load(getNextImage())
                .crossFade(CROSSFADE_DURATION_MS)
                .into(imageView);
    }

    @Override
    public void onSwipeRight() {
        Glide.with(this)
                .load(getPreviousImage())
                .crossFade(CROSSFADE_DURATION_MS)
                .into(imageView);
    }

    @Override
    public void onSwipeTop() {
        // do nothing
    }

    @Override
    public void onSwipeBottom() {
        // do nothing
    }
}
