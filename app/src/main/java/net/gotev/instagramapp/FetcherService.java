package net.gotev.instagramapp;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import com.getinch.retrogram.Instagram;
import com.getinch.retrogram.model.Media;
import com.getinch.retrogram.model.Recent;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;

/**
 * Fetches contents from instagram and saves them on the device.
 * @author Aleksandar Gotev
 */
public class FetcherService extends IntentService {

    private static final int PERIODIC_CHECK_ID = 0;

    protected static final String JPG = ".jpg";
    private static final String ACTION_FETCH_IMAGES = "net.gotev.instagramapp.action.fetch_images";

    private OkHttpClient okHttpClient;
    private PowerManager.WakeLock wakeLock;

    public FetcherService() {
        super(FetcherService.class.getSimpleName());
    }

    private static Intent getFetchImagesIntent(Context context) {
        Intent intent = new Intent(context, FetcherService.class);
        intent.setAction(ACTION_FETCH_IMAGES);
        return intent;
    }

    private static PendingIntent getPeriodicCheckIntent(Context context) {
        return PendingIntent.getService(context, PERIODIC_CHECK_ID, getFetchImagesIntent(context), 0);
    }

    public static void fetchImages(Context context) {
        context.startService(getFetchImagesIntent(context));
    }

    public static void schedulePeriodicCheckEvery(Context context, int millis) {
        Log.d(FetcherService.class.getSimpleName(), "Scheduling periodic check every " + millis + " ms");

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                                  System.currentTimeMillis() + millis,
                                  millis, getPeriodicCheckIntent(context));
    }

    public static void clearPeriodicCheck(Context context) {
        Log.d(FetcherService.class.getSimpleName(), "Clearing scheduled periodic checks...");
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(getPeriodicCheckIntent(context));
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null)
            return;

        Instagram instagram = Util.getInstagram(this);

        if (instagram == null) {
            Log.i(getClass().getSimpleName(), "No instagram access token, exiting");
            return;
        }

        final String action = intent.getAction();

        acquireWakeLock();

        if (ACTION_FETCH_IMAGES.equals(action)) {
            handleFetchImages(instagram);
        }

        releaseWakeLock();
    }

    private void acquireWakeLock() {
        if (wakeLock == null) {
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getSimpleName());
        }

        if (wakeLock != null && !wakeLock.isHeld())
            wakeLock.acquire();
    }

    private void releaseWakeLock() {
        if (wakeLock != null && wakeLock.isHeld())
            wakeLock.release();
    }

    private void handleFetchImages(Instagram instagram) {
        Log.d(getClass().getSimpleName(), "Fetching new images...");
        Recent recent = instagram.getUsersEndpoint().getRecent("self",
                                                               Settings.getNumberOfRecents(this),
                                                               null, null, null, null);

        if (recent == null) {
            Log.d(getClass().getSimpleName(), "Recents is null");
            return;
        }

        boolean thereAreNewImages = false;

        for (Media media : recent.getMediaList()) {
            String thumbnailImageUrl = media.getImages().getThumbnail().getUrl();
            String imageUrl = media.getImages().getStandardResolution().getUrl();

            File thumbnail = new File(Util.getStorageDir(Util.THUMBNAILS_DIR), media.getId() + JPG);
            File image = new File(Util.getStorageDir(Util.IMAGES_DIR), media.getId() + JPG);

            if (!thumbnail.exists()) {
                try {
                    download(thumbnailImageUrl, thumbnail);
                    thereAreNewImages = true;
                } catch (Exception exc) {
                    Log.e(getClass().getSimpleName(),
                          "Error while downloading: " + thumbnailImageUrl, exc);
                }
            }

            if (!image.exists()) {
                try {
                    download(imageUrl, image);
                    thereAreNewImages = true;
                } catch (Exception exc) {
                    Log.e(getClass().getSimpleName(),
                            "Error while downloading: " + thumbnailImageUrl, exc);
                }
            }

        }

        if (thereAreNewImages) {
            Log.d(getClass().getSimpleName(), "Sending broadcast to notify new images");
            sendBroadcast(new Intent().setAction(Settings.NEW_IMAGES_BROADCAST_ACTION));
        } else {
            Log.d(getClass().getSimpleName(), "No new images have been fetched");
        }
    }

    private OkHttpClient getHttpClient() {
        if (okHttpClient == null) {
            okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .build();
        }

        return okHttpClient;
    }

    private void download(String url, File destFile) throws IOException {
        Log.d(getClass().getSimpleName(), "Started download of: " + url);
        Request request = new Request.Builder()
                .url(url)
                .addHeader("access_token", Settings.getAccessToken(this))
                .build();

        BufferedSource source = null;
        BufferedSink sink = null;

        try {
            Response response = getHttpClient().newCall(request).execute();
            source = response.body().source();

            sink = Okio.buffer(Okio.sink(destFile));
            sink.writeAll(source);
            Log.d(getClass().getSimpleName(), "Completed download of: " + url);

        } finally {
            if (source != null) {
                try {
                    source.close();
                } catch (Exception ex) {
                    Log.e(getClass().getSimpleName(), "Error while closing source", ex);
                }
            }

            if (sink != null) {
                try {
                    sink.flush();
                    sink.close();
                } catch (Exception ex) {
                    Log.e(getClass().getSimpleName(), "Error while closing sink", ex);
                }
            }
        }
    }

}
