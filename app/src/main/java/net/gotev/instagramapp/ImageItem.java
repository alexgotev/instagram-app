package net.gotev.instagramapp;

import android.graphics.Bitmap;

/**
 * @author Aleksandar Gotev
 */
public class ImageItem {
    private Bitmap image;
    private String fileName;

    public ImageItem(Bitmap image, String fileName) {
        super();
        this.image = image;
        this.fileName = fileName;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getFileName() {
        return fileName;
    }

}
