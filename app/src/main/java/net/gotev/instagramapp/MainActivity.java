package net.gotev.instagramapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSIONS_REQUEST_CODE = 1;
    private AndroidPermissions mPermissions;
    private Button loginButton;
    private GridView gridView;
    private GridViewAdapter gridAdapter;

    private FetcherServiceReceiver fetcherReceiver = new FetcherServiceReceiver() {
        @Override
        public void onImagesFetched(Context context) {
            Log.d(MainActivity.class.getSimpleName(), "Notified that new images were fetched");
            gridAdapter = new GridViewAdapter(MainActivity.this, R.layout.grid_item_layout, Util.getThumbnails());
            gridView.setAdapter(gridAdapter);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginButton = (Button) findViewById(R.id.loginButton);
        gridView = (GridView) findViewById(R.id.gridView);
        gridAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, Util.getThumbnails());
        gridView.setAdapter(gridAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onImageClick((ImageItem) gridAdapter.getItem(position));
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoginClick(v);
            }
        });

        mPermissions = new AndroidPermissions(this,
                Manifest.permission.INTERNET,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        handleOAuthIntent(getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem logout = menu.findItem(R.id.menu_logout);
        logout.setVisible(!Settings.getAccessToken(this).isEmpty());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.menu_logout) {
            Util.showYesNoDialog(this, R.string.confirm_logout, new Util.YesNoListener() {
                @Override
                public void onYes() {
                    performLogout();
                }

                @Override
                public void onNo() {
                    //Do nothing
                }
            });
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Util.setBroadcastReceiverEnabled(this, GlobalFetcherReceiver.class, false);
        fetcherReceiver.register(this);

        if (Settings.getAccessToken(this).isEmpty()) {
            showLoginButton();
        } else {
            hideLoginButtonAndShowImages();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        Util.setBroadcastReceiverEnabled(this, GlobalFetcherReceiver.class, true);
        fetcherReceiver.unregister(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        handleOAuthIntent(intent);
    }

    private void onLoginClick(View view) {
        if (mPermissions.checkPermissions()) {
            performLogin();
        } else {
            Log.i(getClass().getSimpleName(), "Some needed permissions are missing. Requesting them.");
            mPermissions.requestPermissions(PERMISSIONS_REQUEST_CODE);
        }
    }

    private void onImageClick(ImageItem imageItem) {
        DetailsActivity.open(this, imageItem.getFileName());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.i(getClass().getSimpleName(), "onRequestPermissionsResult");

        if (mPermissions.areAllRequiredPermissionsGranted(grantResults)) {
            performLogin();
        } else {
            onInsufficientPermissions();
        }
    }

    private void onInsufficientPermissions() {
        Toast.makeText(this, getString(R.string.insufficient_permissions), Toast.LENGTH_LONG).show();
    }

    private void performLogin() {
        if (!Util.startInstagramExplicitOAuthFlow(this)) {
            Toast.makeText(this, getString(R.string.no_instagram_config), Toast.LENGTH_LONG).show();
        }
    }

    private void performLogout() {
        Settings.clearAccessToken(this);
        showLoginButton();
        invalidateOptionsMenu();
        getSupportActionBar().invalidateOptionsMenu();
        FetcherService.clearPeriodicCheck(this);
        Util.clearStorageDir(Util.IMAGES_DIR);
        Util.clearStorageDir(Util.THUMBNAILS_DIR);
    }

    private void showLoginButton() {
        loginButton.setVisibility(View.VISIBLE);
        gridView.setVisibility(View.GONE);
    }

    private void hideLoginButtonAndShowImages() {
        loginButton.setVisibility(View.GONE);
        gridView.setVisibility(View.VISIBLE);
    }

    private void handleOAuthIntent(Intent intent) {
        if (intent == null || !Intent.ACTION_VIEW.equals(intent.getAction()))
            return;

        Uri uri = intent.getData();
        String scheme = uri.getScheme();

        if (!scheme.equals(Settings.APP_SCHEME))
            return;

        String status = uri.getPathSegments().get(0);
        intent.setAction(""); //needed to "consume" the intent, which otherwise gets redelivered

        if ("error".equals(status)) {
            onOAuthError(uri.getPathSegments().get(1));
        } else if ("success".equals(status)) {
            onOAuthSuccess(uri.getPathSegments().get(1));
        } else {
            Log.e(getClass().getSimpleName(), "Unknown status: " + status);
        }
    }

    private void onOAuthSuccess(String token) {
        Log.i("OAuth", "Received Access Token: " + token);
        Settings.saveAccessToken(this, token);
        FetcherService.fetchImages(this);
        FetcherService.schedulePeriodicCheckEvery(this, Settings.getUpdateIntervalInMillis(this));
    }

    private void onOAuthError(String error) {
        Log.e("OAuth", "Error: " + error);
        Toast.makeText(this, getString(R.string.login_cancelled), Toast.LENGTH_LONG).show();
    }
}
