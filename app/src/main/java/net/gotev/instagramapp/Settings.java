package net.gotev.instagramapp;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Aleksandar Gotev
 */
public class Settings {

    // if you change this, be sure to update AndroidManifest.xml as well, and also the server-side
    // as it's needed in order to perform Explicit OAuth authentication flow.
    public static final String APP_SCHEME = "offlinephotobrowser";

    public static final String NAMESPACE = "net.gotev.instagramapp";

    // if you change this, be sure to update GlobalBroadcastReceiver in AndroidManifest.xml as well
    public static final String NEW_IMAGES_BROADCAST_ACTION = NAMESPACE + ".newimages";

    public static final String INSTAGRAM_CLIENT_ID = "0b717004ea3249e4bcbba5f9a119f40e";
    public static final String INSTAGRAM_AUTHORIZATION_REDIRECT_URI =
            "http://www.gotev.net/offlinephotobrowser/oauth.php";

    private static final String PREFS_FILE_NAME = "instagramapp-prefs.xml";
    private static final String KEY_TOKEN = NAMESPACE + ".token";
    private static final String KEY_RECENTS = NAMESPACE + ".recents";
    private static final String KEY_UPDATE_INTERVAL = NAMESPACE + ".update_interval";

    private static SharedPreferences prefsInstance;

    private static SharedPreferences getPrefs(Context context) {
        if (prefsInstance == null) {
            prefsInstance = context.getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE);
        }
        return prefsInstance;
    }

    public static String getAccessToken(Context context) {
        return getPrefs(context).getString(KEY_TOKEN, "");
    }

    public static void saveAccessToken(Context context, String token) {
        getPrefs(context).edit().putString(KEY_TOKEN, token).commit();
    }

    public static int getNumberOfRecents(Context context) {
        return getPrefs(context).getInt(KEY_RECENTS, 50);
    }

    public static void saveNumberOfRecents(Context context, int number) {
        getPrefs(context).edit().putInt(KEY_RECENTS, number).commit();
    }

    public static int getUpdateIntervalInMillis(Context context) {
        return (getPrefs(context).getInt(KEY_UPDATE_INTERVAL, 15) * 60 * 1000);
    }

    public static void saveUpdateIntervalInMinutes(Context context, int number) {
        getPrefs(context).edit().putInt(KEY_UPDATE_INTERVAL, number).commit();
    }

    public static void clearAccessToken(Context context) {
        getPrefs(context).edit().remove(KEY_TOKEN).commit();
    }

    public static String getInstagramOAuthURL() {
        return "https://api.instagram.com/oauth/authorize/?client_id=" + INSTAGRAM_CLIENT_ID +
                "&redirect_uri=" + INSTAGRAM_AUTHORIZATION_REDIRECT_URI + "&response_type=code";
    }
}
