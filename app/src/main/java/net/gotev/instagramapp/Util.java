package net.gotev.instagramapp;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.getinch.retrogram.Instagram;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

/**
 * @author Aleksandar Gotev
 */
public class Util {

    public interface YesNoListener {
        void onYes();
        void onNo();
    }

    protected static final String BASE_DIR = "InstagramApp";
    protected static final String THUMBNAILS_DIR = "thumbnails";
    protected static final String IMAGES_DIR = "images";

    private static Instagram instagramInstance;

    public static boolean startInstagramExplicitOAuthFlow(Context context) {
        String url = Settings.getInstagramOAuthURL();

        if (url == null) {
            return false;
        }

        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        return true;
    }

    public static Instagram getInstagram(Context context) {
        if (instagramInstance == null) {
            instagramInstance = new Instagram(Settings.getAccessToken(context));
        }

        return instagramInstance;
    }

    public static File getStorageDir(String dirName) {
        File baseDir = new File(Environment.getExternalStorageDirectory(), BASE_DIR);
        File dir = new File(baseDir, dirName);
        if (!dir.exists())
            if (!dir.mkdirs())
                Log.e("Util", "Unable to create dir: " + dir.getAbsolutePath());

        return dir;
    }

    public static void clearStorageDir(String dirName) {
        File baseDir = new File(Environment.getExternalStorageDirectory(), BASE_DIR);
        File dir = new File(baseDir, dirName);
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String file : children) {
                new File(dir, file).delete();
            }
        }
    }

    public static ArrayList<ImageItem> getThumbnails() {
        File thumbnailsDir = Util.getStorageDir(THUMBNAILS_DIR);
        String[] files = thumbnailsDir.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return (filename.endsWith(FetcherService.JPG));
            }
        });

        if (files == null || files.length == 0)
            return new ArrayList<>(1);

        ArrayList<ImageItem> bitmaps = new ArrayList<>(files.length);

        for (String file : files) {
            String path = new File(thumbnailsDir, file).getAbsolutePath();
            ImageItem item = new ImageItem(BitmapFactory.decodeFile(path), file);
            bitmaps.add(item);
        }

        return bitmaps;
    }

    public static String[] getImageFileNames() {
        return Util.getStorageDir(IMAGES_DIR).list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return (filename.endsWith(FetcherService.JPG));
            }
        });
    }

    public static void showYesNoDialog(Context context, int messageId, final YesNoListener listener) {
        DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        listener.onYes();
                        break;

                    default:
                        listener.onNo();
                        break;
                }
            }
        };

        new AlertDialog.Builder(context)
                .setMessage(context.getString(messageId))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.yes), dialogListener)
                .setNegativeButton(context.getString(R.string.no), dialogListener)
                .show();
    }

    public static void setBroadcastReceiverEnabled(Context context,
                                                   Class<? extends BroadcastReceiver> receiver,
                                                   boolean enabled) {
        Log.d(Util.class.getSimpleName(), (enabled ? "enabling " : "disabling ") + receiver.getName());
        int status = enabled ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED
                             : PackageManager.COMPONENT_ENABLED_STATE_DISABLED;

        PackageManager pm  = context.getPackageManager();
        ComponentName componentName = new ComponentName(context, receiver);
        pm.setComponentEnabledSetting(componentName, status, PackageManager.DONT_KILL_APP);
    }
}
