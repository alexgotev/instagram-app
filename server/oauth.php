<?php
define("CLIENT_ID", "0b717004ea3249e4bcbba5f9a119f40e");
define("CLIENT_SECRET", "e84d54615c4e442e99a28042710e4a76");
define("AUTHORIZATION_REDIRECT_URI", "http://www.gotev.net/offlinephotobrowser/oauth.php");
define("APP_REDIRECT_SCHEME", "offlinephotobrowser://response/");

if (!isset($_REQUEST['code'])) {
    if (isset($_REQUEST['error'])) {
        header('Location: ' . APP_REDIRECT_SCHEME . 'error/' . $_REQUEST['error'] . '/' . $_REQUEST['error_reason']);
    } else {
        http_response_code(400);
    }
    exit();
}

// Define URL where the form resides
$form_url = "https://api.instagram.com/oauth/access_token";

// This is the data to POST to the form. The KEY of the array is the name of the field. The value is the value posted.
$data_to_post = array (
    'client_id' => CLIENT_ID,
    'client_secret' => CLIENT_SECRET,
    'grant_type' => 'authorization_code',
    'redirect_uri' => AUTHORIZATION_REDIRECT_URI,
    'code' => $_REQUEST['code']
);

// Initialize cURL
$curl = curl_init();

// Set the options
curl_setopt($curl,CURLOPT_URL, $form_url);

// This sets the number of fields to post
curl_setopt($curl,CURLOPT_POST, sizeof($data_to_post));

// This is the fields to post in the form of an array.
curl_setopt($curl,CURLOPT_POSTFIELDS, $data_to_post);

curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);

//execute the post
$result = curl_exec($curl);

//close the connection
curl_close($curl);

if (isset($result) && !empty($result)) {
    $json = json_decode($result);
    header('Location: ' . APP_REDIRECT_SCHEME . 'success/' . $json->{'access_token'});
    exit();

} else {
    $error = "invalid_post_response";
    if (isset($_REQUEST['error'])) {
        $error = $_REQUEST['error'];
    }

    $error_reason = "invalid_post_response";
    if (isset($_REQUEST['error_reason'])) {
        $error_reason = $_REQUEST['error_reason'];
    }

    header('Location: ' . APP_REDIRECT_SCHEME . 'error/' . $error . '/' . $error_reason);
    exit();
}

http_response_code(403);
?>
